let io = require('socket.io');
let promise = null;

module.exports = {
  connect(server){
    io = io(server);
    promise = new Promise(function(resolve, reject) {
      io.once('connect', function(socket) {
        resolve(socket);
      });
      io.once('connect_error', function() {
        reject(new Error('connect_error'));
      });
      io.once('connect_timeout', function() {
        reject(new Error('connect_timeout'));
      });
    });
  },
  emit(event, data, owner = null) {
    if (promise === null) {
      throw Error('You try use socket before connect to server.');
    }

    promise.then((socket) => {
      if (!owner) {
        return socket.emit(event, data);
      }
      const clients = io.sockets.sockets;
      Object.keys(clients).forEach((client) => {
        client !== owner && io.to(client).emit(event, data);
      });
    });
  }
};
