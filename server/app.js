const express = require('express');
require('dotenv').config({ path: __dirname + '/.env' });
const { connect } = require('./socket');

const placesRouter = require('./routes/todoList');
const bodyParser  = require('body-parser');

const app = express();

const allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  res.header("Access-Control-Allow-Headers", 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

  next();
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(allowCrossDomain);

app.use('/todo', placesRouter);

const server = app.listen(process.env.PORT, function () {
  console.log(`Example app listening on port ${process.env.PORT}!`);
});

try {
  connect(server);
} catch(error) {
  console.error(error);
}

