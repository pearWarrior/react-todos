const router = require('express').Router();
let list = require('../list.json');
const EVENTS = require('./events');
const { emit } = require('../socket');

let lastId = list.length;

const DEFAULT_ITEM = {
  description: '',
  status: false
};

function getId() {
  ++lastId;
  if (!Number.isSafeInteger(lastId)) {
    throw Error('Id got max value');
  }
  return lastId;
}

router.get('/', async(req, res, next) => {
  try {
    res.status(200).send('OK');
  } catch (error) {
    next(error);
  }
});

router.get('/list', async(req, res, next) => {
  try {
    res.status(200).send(list);
  } catch (error) {
    next(error);
  }
});

router.patch('/:id', async(req, res, next) => {
  try {
    const id = list.findIndex(item => item._id === Number(req.params.id));
    const changedItem =  { ...list[id], ...req.body };
    list[id] = changedItem;
    emit(EVENTS.CHANGE, changedItem, req.body.clientId);
    res.send('Done');
  } catch (error) {
    next(error);
  }
});

router.delete('/:id/:clientId', async(req, res, next) => {
  try {
    const id = list.findIndex(item => item._id === Number(req.params.id));
    list.splice(id, 1);
    emit(EVENTS.DELETE, req.params.id, req.params.clientId);
    res.send('Done');
  } catch (error) {
    next(error);
  }
});

router.post('/create', async(req, res, next) => {
  try {
    const newItem = { ...DEFAULT_ITEM, description: req.body.description, _id: getId() };
    list.push(newItem);
    emit(EVENTS.CREATE, newItem, req.body.clientId);
    res.json(newItem);
  } catch (error) {
    next(error);
  }
});

module.exports = router;