module.exports = {
  CREATE: 'todo-create',
  DELETE: 'todo-delete',
  CHANGE: 'todo-change',
};
