import io from 'socket.io-client';
let connectPromise = null;

export function connect() {
  const socket = io(process.env.REACT_APP_API_URL);

  connectPromise = new Promise((resolve, reject) => {
    socket.on('connect', function() {
      resolve(socket);
    });

    socket.on('reconnect', function() {
      resolve(socket);
    });

    socket.on('error', function(error) {
      reject(error);
    });
  });
  return connectPromise;
}

export function getSocketId() {
  if (!connectPromise) {
    throw new Error('Socket try use before connect call.');
  }
  return connectPromise.then(connect => connect.io.engine.id);
}