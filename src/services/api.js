import { Axios } from 'axios';
import { getSocketId } from './socket';

const TODO_ROUTE = '/todo';

class Api extends Axios {
  constructor() {
    super({ baseURL: process.env.REACT_APP_API_URL });
  }

  tasks(params) {
    return this.get(`${TODO_ROUTE}/list`, { params });
  }

  async updateTask(id, data) {
    const item = { ...data , clientId: await getSocketId() };
    return this.patch(`${TODO_ROUTE}/${id}`, item);
  }

  async createTask(data) {
    return this.post(`${TODO_ROUTE}/create`, { ...data , clientId: await getSocketId() });
  }

  async removeTask(id) {
    return this.delete(`${TODO_ROUTE}/${id}/${await getSocketId()}`);
  }
}

export default new Api();