import React from 'react';
import ListItem from './ListItem/ListItem';
import Loader from 'react-loader-advanced';
import './List.css';

class List extends React.PureComponent {
  render() {
    const { items, ...props } = this.props;
    return (
      <div className="container">
        { items.map((item, index) =>
          <Loader key={index} show={item.update || false} message="updating...">
            <ListItem {...props} key={item._id} item={item}/>
          </Loader>
        )}
      </div>
    );
  }
}

export default List;
