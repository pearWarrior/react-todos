import React from 'react';
import './ListItem.css';
import { debounce } from 'lodash';

class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      status: this.props.item.status,
      description: this.props.item.description,
    };

    this.handleSave = this.handleSave.bind(this);
    this.changeDescription = this.changeDescription.bind(this);
    this.handleStatus = this.handleStatus.bind(this);
    this.handleDelete = this.handleDelete.bind(this);

    this.saveDebounced = debounce(this.handleSave, 600);
  }

  componentWillReceiveProps({ item }) {
    if (item !== this.props.item) {
      this.setState({ description: item.description, status: item.status });
    }
  }

  handleSave() {
    const { _id } = this.props.item;
    this.props.updateTask(_id, this.state);
  }

  handleDelete() {
    const { _id } = this.props.item;
    this.props.removeTask(_id);
  }

  changeDescription({ target }) {
    this.setState({ description: target.value });
    this.saveDebounced();
  }

  handleStatus() {
    this.setState({ status: !this.state.status });
    this.saveDebounced();
  }

  render() {
    const { status, description } = this.state;

    return (
      <div className="row">
        <div className="checkbox column">
          <input type="checkbox" className="align-bottom" onChange={this.handleStatus} checked={status}/>
        </div>
        <div className="column description">
          <input type="text" className="form-control" value={description} onChange={this.changeDescription}/>
        </div>
        <div className="column">
          <button type="button" className="btn-sm btn-danger" onClick={this.handleDelete}>Delete</button>
        </div>
      </div>
    );
  }
}

export default ListItem;