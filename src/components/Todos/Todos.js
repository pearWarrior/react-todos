import React from 'react';
import { connect } from 'react-redux';
import CreateTask from './CreateTask/CreateTask';
import List from '../List/List';
import { connect as socketConnect } from '../../services/socket';
import EVENTS from '../../services/events';
import { fetchTasks, createTask, updateTask, removeTask, createTaskImmediately, removeTaskImmediately, updateTaskImmediately } from '../../actions/items';

class Todos extends React.Component {

  constructor(props) {
    super(props);
    socketConnect().then((socket) => {
      socket.on(EVENTS.CREATE, this.props.createTaskImmediately);
      socket.on(EVENTS.DELETE, this.props.removeTaskImmediately);
      socket.on(EVENTS.CHANGE, this.props.updateTaskImmediately);

      socket.on('reconnect', () => {
        this.props.fetchData();
      });
    });
  }

  componentDidMount() {
    this.props.fetchData();
  }

  render() {
    console.log('todo rebuild', this.props.items);
    return (
      <div>
        <CreateTask createTask={this.props.createTask}/>
        <hr/>
        <List updateTask={this.props.updateTask} removeTask={this.props.removeTask} items={this.props.items}/>
      </div>
    );
  }
}

const mapStateToProps = ({ items }) => ({ items });

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: () => dispatch(fetchTasks()),
    createTask: (description) => dispatch(createTask(description)),
    updateTask: (id, data) => dispatch(updateTask(id, data)),
    removeTask: (id) => dispatch(removeTask(id)),
    createTaskImmediately: data => dispatch(createTaskImmediately(data)),
    removeTaskImmediately: id => dispatch(removeTaskImmediately(id)),
    updateTaskImmediately: data => dispatch(updateTaskImmediately(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
