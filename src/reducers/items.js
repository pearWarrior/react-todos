import { TYPES } from '../actions/items';

export function items(state = [], action) {
  let index;
  let items;

  const findTask = (id) => state.findIndex((task) => task._id === id);

  switch (action.type) {
    case TYPES.TASKS_FETCH_END:
      return action.items;

    case TYPES.TASKS_RESET:
      return [];

    case TYPES.TASK.UPDATE:
      index = findTask(action.payload.id);

      if (index === -1) {
        return state;
      }
      state[index].update = action.payload.status;
      return [...state];

    case TYPES.TASK.CHANGED:
      index = findTask(action.item._id);

      if (index === -1) {
        return state;
      }
      state[index] = Object.assign({}, action.item);
      return state.slice(0);

    case TYPES.TASK.CREATED:
      state.push(action.item);
      return [...state];

    case TYPES.TASK.REMOVED:
      index = findTask(Number(action.id));

      if (index === -1) {
        return state;
      }

      state.splice(index, 1);
      return [...state];

    default:
      return state;
  }
}